cd "/c/Program Files (x86)/National Instruments/LabVIEW 2013/user.lib" &&
git clone -o Gitorious "git@gitorious.gsi.de:brand/ascii.git" ASCII &&
cd ASCII &&
git checkout -b TS2013 &&
git status || echo " Cloning git repository failed: ASCII"

cd "/c/Program Files (x86)/National Instruments/LabVIEW 2013/instr.lib" &&
git clone -o Gitorious "git@gitorious.gsi.de:brand/hhnd2xx.git" HHND2xx &&
cd HHND2xx &&
git checkout -b TS2013 &&
git status || echo " Cloning git repository failed: HHND2xx"

cd "/c/Program Files (x86)/National Instruments/LabVIEW 2013/instr.lib" &&
git clone -o Gitorious "git@gitorious.gsi.de:brand/SMxxx.git" SMxxx &&
cd SMxxx &&
git checkout -b TS2013 &&
git status || echo " Cloning git repository failed: SMxxx"
